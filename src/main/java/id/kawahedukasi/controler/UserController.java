package id.kawahedukasi.controler;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Map;

@Path("/users")
@Produces (MediaType.APPLICATION_JSON)
public class UserController {
    @POST
    public Map<String, Object> create(){
        return Map.of(
                "message","CREATED"
        );
    }

    @GET
    public Map<String, Object> GET(){
        return Map.of(
                "message","GET"
        );
    }

    @PUT
    public Map<String, Object> update(){
        return Map.of(
                "message","PUT"
        );
    }

    @DELETE
    public Map<String, Object> delete (){
        return Map.of(
                "message","DELETE"
        );
    }
}
